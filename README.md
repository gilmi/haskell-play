# [nyx-game](https://gilmi.me/nyx)

A bullet hell game written in Haskell.

Click 'R' 5 times to expose a secret menu.

## How to Run

### From Source

You will need [Stack](https://haskellstack.org).

#### Ubuntu

```sh
sudo apt install libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev libsdl2-mixer-dev
make build
make exec
```

#### OS X

```sh
brew install sdl2 sdl2_ttf sdl2_image sdl2_mixer
make build
make exec
```

#### Windows

```sh
stack exec -- pacman -Syu
stack exec -- pacman -S mingw-w64-x86_64-pkg-config mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image mingw-w64-x86_64-SDL2_ttf mingw-w64-x86_64-SDL2_mixer
stack build
```
